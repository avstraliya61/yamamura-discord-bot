|                                                                                                                                                                                                                                                                         [![Logo](https://yamamura-bot.tk/logo.png "Yamamura")][https://yamamura-bot.tk]                                                                                                                                                                                                                                                                         |
|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| ![Owner](https://discordbots.org/api/widget/owner/421158339129638933.svg "NightYoshi370 - Yamamura Owner") ![Status](https://discordbots.org/api/widget/status/421158339129638933.svg "Bot Status") ![Upvotes](https://discordbots.org/api/widget/upvotes/421158339129638933.svg "Bot Upvotes") ![Server Count](https://discordbots.org/api/widget/servers/421158339129638933.svg "The amount of servers the bot is in") ![Library](https://discordbots.org/api/widget/lib/421158339129638933.svg "The language the bot is coded in")             <img  src = "https://discordbots.org/api/widget/lib/421158339129638933.svg" > |

Yamamura is an all-in-one Discord bot dedicated to helping modding communities and more.   
It can fulfill your server's moderation needs and create fun events for your community to enjoy

Here are a list of features it can do outside of commands:

- Handle a StarBoard
- Server Experience Points
- Logging Server Events with things that the Discord Audit Logs do not log for you
- A group of Welcome and Leave messages, picked at random
- Listen to audio on Youtube
- A website in which you can access at any time to see all what Yamamura has to offer

That isn't even talking about the commands which you can perform on the bot. There are many to be offered, with some exclusives being:

- Showcase your very own Super Mario Maker (1) course
- Connecting to a makerboard website
- Edit any image you throw at the bot with memes and game covers
- Organize your server moderation, with things such as warning, message prune and message moving
- Get information on Game Servers
- ...and much more!

Check out our website to find more information: https://yamamura-bot.tk/

## Bug Reports, Support, Feedback and More

Please join our [Discord Server](https://discord.gg/vbYZCRZ) for discussion on these things

## Your own local instance

If you'd just like to invite the Bot to the server, just use the invite found on the website.
In order to have your own Yamamura, you will need to have a server that can run node.js. Then clone this git repository, run npm init and then run yamamura.js

## Helping out

Do you want to help make Yamamura the best bot possible?!
If so, here's how you can help us out:

- Find a bug? Report it! Our developers will take care of it as soon as possible.
- Spread the word! The more servers the bot is in, there will be more people to experience the bot.
- Join our [Discord Server](https://discord.gg/vbYZCRZ)! You can express your opinion about the bot there as well as partake in development.
- Let us know what you think of the bot, and what we should add or change! We take your opinion seriously.
- Upvote the bots on [discordbotlist.com](https://discordbotlist.com/bots/421158339129638933), [discordbots.org](https://discordbots.org/bot/421158339129638933) & [discord.boats](https://discord.boats/bot/421158339129638933).
- If you're a programmer, you can help out with Yamamura development sending pull requests on our [GitLab repository](https://gitlab.com/Samplasion/yamamura-discord-bot). We're currently looking for help in regards to code cleaning and adding highly requested and complex features. We use Node.JS as the language backend alongside Discord.JS with Discord-Akairo.
- Help [JeDaYoshi#7942](https://jedayoshi.com) pay for a server by donating to his [PayPal](https://paypal.me/Naydire). Currently, Yamamura is running on a VPS he's borrowing, but by donating, you can help maintain the server and buy a better one.
- Documentation is quite sparse at the moment. We need you to help fill it out by documenting what each command does and give descriptions to the command parameters. You can do so by Sending a Pull Request on our GitLab repository.
- Do you know another language? Help translate Yamamura.

If you need anything, join the support server found on our website. We hope to see you soon!
~ NightYoshi370, owner of Yamamura