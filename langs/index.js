module.exports = [
  {
    code: "en",
    name: "English",
    flag: ":flag_us:",
    translators: ["NightYoshi370#5597", "Samplasion#0325"]
  },
  {
    code: "fr",
    name: "Français (French)",
    flag: ":flag_fr:",
    translators: ["NightYoshi370#5597"]
  },
  {
    code: "it",
    name: "Italiano (Italian)",
    flag: ":flag_it:",
    translators: ["Samplasion#0325"]
  }
];