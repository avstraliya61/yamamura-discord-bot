const Command = require('../../struct/Command');

module.exports = class MCBedrockCommand extends Command {
  constructor() {
    super('minecraft-be', {
      category: 'Game Server Statistics',
      aliases: ["minecraft-be", "mcbe"],
      clientPermissions: ['EMBED_LINKS'],
      description: 'Get stats of any Minecraft: Bedrock Edition game server.',
      args: [
        {
          id: 'IP',
          prompt: {
            start: 'Which server would you like to get `Minecraft: Bedrock Edition` statistics from?',
            retry: 'That\'s not a server we can get stats from! Try again.'
          },
          type: 'string',
          match: 'content'
        }
      ]
    });

    this.examples = ['minecraft-be 138.201.33.99:19132'];
  }

  async exec(message, { IP }) {
    let { embed, data } = await this.gameDigServer('minecraftbe', IP);
    embed
      .setThumbnail(`${this.client.website.URL}/icons/minecraft.png`)
      .setColor("GREEN")

    let text = `Information on the "${data.name}" Minecraft (Bedrock Edition) server`;
    if (message.guild)
      text += `, requested by ${message.member.displayName}`

    message.util.send(text, {embed});
  }
};